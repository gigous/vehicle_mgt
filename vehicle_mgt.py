from uuid import uuid4              # For random, unique VINs
from collections import namedtuple  # For Manufacturer
from abc import ABC

# rather than a class, use a namedtuple for Manufacturer (it's just data)
Manufacturer = namedtuple('Manufacturer', 'name, country')

class Vehicle(object):
    """Describes a vehicle

    Positional Arguments
    mfr -- vehicle Manufacturer namedtuple with strings name and country
    model -- vehicle model as a string
    year -- vehicle year as an integer
    color -- vehicle color as a string
    """
    def __init__(self, mfr, model, year, color):
        self._vin = uuid4() # Generate unique vin
        self._mfr = mfr
        self._model = model
        self._year = year
        self._color = color


    def __str__(self):
        #return (f"{self._color.capitalize()} {self._year} {self._model} mfd. "
                 #"by {} in {}".format(self._mfr.name, self._mfr.country))
        s = (f"{self._color.capitalize()} {self._year} {self._model} mfd. "
              "by {} in {}".format(self._mfr.name, self._mfr.country))
        if self._mfr.name == "BMW":
            s += " © BMW AG, Munich, Germany"
        elif self._mfr.name == "Tesla":
            s += " (Batteries Included!)"
        return s


    @property
    def vin(self):
        return self._vin

    @property
    def mfr(self):
        return self._mfr

    @property
    def model(self):
        return self._model

    @property
    def year(self):
        return self._year

    @property
    def color(self):
        return self._color


class VehicleSpec(ABC):
    """Abstract vehicle specification
    """
    def is_satisfied(self, vehicle):
        """Abstract function to check if a vehicle has satisfied the
        specification
        """
        return NotImplemented

class VehicleYearSpec(VehicleSpec):
    """Specification for the year of the vehicle
    """
    def __init__(self, year):
        self._year = year

    def is_satisfied(self, vehicle):
        return self._year == vehicle.year


class VehicleMfrNameSpec(VehicleSpec):
    """Specification for the Manufacturer of the vehicle

    mfr_name -- the name of the Manufacturer
    """
    def __init__(self, mfr_name):
        self._mfr_name = mfr_name

    def is_satisfied(self, vehicle):
        return self._mfr_name == vehicle.mfr.name


class VehicleRepo(object):
    """An inventory of vehicles

    vehicles -- an initial list of Vehicles
    """
    def __init__(self, vehicles=None):
        if vehicles is None:
            self._vehicles = set()
        else:
            self._vehicles = set(vehicles)

    def __str__(self):
        s = "[\n"
        for v in self.inventory:
            s += f"\t{v}\n"
        s += "]"
        return s

    @property
    def inventory(self):
        return list(self._vehicles)

    def find_vehicle(self, vin):
        for v in self._vehicles:
            if vin == v.vin:
                return v
        return None

    def add_vehicle(self, vehicle):
        self._vehicles.add(vehicle)

    def remove_vehicle(self, vin):
        vehicle = self.find_vehicle(vin)
        # This could probably be handled differently, but
        # is here for basic error checking
        if vehicle is None:
            raise KeyError(f"The vehicle with VIN {vin} is not in the "
                            "inventory")
        self._vehicles.remove(vehicle)


class VehicleFilter(object):
    """Class to filter the VehicleRepo by a specification
    """
    def filter(self, repo, spec):
        """Return a new VehicleRepo with vehicles that meet the spec"""
        filtered_vehicles = VehicleRepo()
        for v in repo.inventory:
            if spec.is_satisfied(v):
                filtered_vehicles.add_vehicle(v)
        return filtered_vehicles


if __name__ == '__main__':
    ford = Manufacturer("Ford", "United States")
    gm = Vehicle(ford, "Grand Marquis", 1998, "dark green")
    print(gm)

    bmw = Manufacturer("BMW", "Germany")
    tesla = Manufacturer("Tesla", "United States")
    chevy = Manufacturer("Chevrolet", "United States")
    toyota = Manufacturer("Toyota", "Japan")
    honda = Manufacturer("Honda", "Japan")

    m2 = Vehicle(bmw, "M2", 2015, "white")
    super_duty = Vehicle(ford, "Super Duty", 2018, "maroon")
    model_s = Vehicle(tesla, "Model S", 2015, "black")
    volt = Vehicle(chevy, "Volt", 2017, "blue")
    prius = Vehicle(toyota, "Prius", 2017, "silver")
    civic = Vehicle(honda, "Civic", 2001, "yellow & black")

    repo = VehicleRepo([m2])
    print(repo)
    repo.add_vehicle(volt)
    repo.add_vehicle(super_duty)
    print(repo)
    repo.remove_vehicle(super_duty.vin) # Nah, it's too loud
    print(repo)
    try:
        repo.remove_vehicle(prius.vin) # throws exception
    except:
        pass
    finally:
        pass
    # add the rest
    repo.add_vehicle(prius)
    repo.add_vehicle(civic)
    repo.add_vehicle(civic) # already exists, not added
    repo.add_vehicle(model_s)
    repo.add_vehicle(super_duty) # Hmmm, but it does have 336 kW horsepower...
    repo.add_vehicle(gm)
    print(repo)

    # Create a couple specification to filter by a year and a manufacturer
    spec_year_2017 = VehicleYearSpec(2017)
    spec_mfr_ford = VehicleMfrNameSpec("Ford")
    # Create filter object
    v_filter = VehicleFilter()

    # Vehicles with model year 2017
    vehicles_year_2017 = v_filter.filter(repo, spec_year_2017)
    print(vehicles_year_2017)
    # Vehicles that are mfd. by Ford
    vehicles_mfr_ford = v_filter.filter(repo, spec_mfr_ford)
    print(vehicles_mfr_ford)
